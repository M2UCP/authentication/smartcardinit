package main

import (
	"fmt"
	"gitlab.com/M2UCP/authentication/authenticator"
	"gitlab.com/M2UCP/authentication/smartcard"
	"time"
)

// read or write?
const IsReadMode = false

// is server?
const IsServer = true

func main( ) {
	// tokens
	serverToken := "6OS4ASS2WC2NU6KT"
	clientToken := "RMKHMXUB7W5KLSU3"

	// build card handler
	if cardHandler, err := card.BuildHandler( ); err == nil {
		for {
			// update smart card handler
			cardHandler.Update( )

			// get last inserted card
			if insertedCard := cardHandler.GetLastConnectedCard( ); insertedCard != nil {
				var cardMode authenticator.CardMode
				if IsServer {
					cardMode = authenticator.CardModeM2MServer
				} else {
					cardMode = authenticator.CardModeM2MClient
				}

				if IsReadMode {
					data, _ := insertedCard.SendRead( 10,
						4 )
					fmt.Print( "Mode (",
						data,
						"): " )
					if data[ 0 ] == byte( authenticator.CardModeM2MClient ) {
						fmt.Println( "client" )
					} else {
						fmt.Println( "server" )
					}

					data, _ = insertedCard.SendRead( 14,
						16 )
					fmt.Println( "Server token:",
						string( data ))

					data, _ = insertedCard.SendRead( 18,
						16 )
					fmt.Println( "Client token:",
						string( data ))
				} else {
					fmt.Println( "Writing card mode and tokens check sums" )
					_ = insertedCard.SendUpdate( 10,
						[ ]byte{ byte( cardMode ),
							authenticator.CalculateChecksum( [ ]byte( serverToken ) ),
							authenticator.CalculateChecksum( [ ]byte( clientToken ) ),
							byte( cardMode ) } )

					fmt.Println( "Writing server token" )
					_ = insertedCard.SendUpdate( 14,
						[ ]byte( serverToken ) )

					fmt.Println( "Writing client token" )
					_ = insertedCard.SendUpdate( 18,
						[ ]byte( clientToken ) )
				}
			}

			// sleep
			time.Sleep( time.Second )
		}
	}
}
